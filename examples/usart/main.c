#include "ll_irq.h"
#include "ll_dma.h"
#include "ll_rcc.h"
#include "ll_gpio.h"
#include "ll_usart.h"

#define PA(n) LL_GPIO_PIN_DEFINE(0, n)
#define PB(n) LL_GPIO_PIN_DEFINE(1, n)
#define PC(n) LL_GPIO_PIN_DEFINE(2, n)
#define PD(n) LL_GPIO_PIN_DEFINE(3, n)
#define PE(n) LL_GPIO_PIN_DEFINE(4, n)

#define COM_USART USART1
#define COM_LL_USART_HANDLE LL_USART1_HANDLE
#define COM_USART_IRQn USART1_IRQn
#define COM_USART_IRQ_HANDLER USART1_IRQHandler
#define COM_USART_TX_DMA DMA1_Channel4
#define COM_USART_TX_DMA_IRQn DMA1_Channel4_IRQn
#define COM_USART_TX_DMA_IRQ_HANDLER DMA1_Channel4_IRQHandler
#define COM_TX_PIN PA(9)
#define COM_RX_PIN PA(10)

void com_put_string(const char *str)
{
    uint8_t *ptr = (uint8_t *)str;
    while (*ptr)
    {
        ll_usart_send_byte_by_polling(COM_LL_USART_HANDLE, *ptr++);
    }
}

#define COM_PUT_CONST_STRING(str) ll_usart_send_bytes_by_dma(COM_LL_USART_HANDLE, str, sizeof(str) - 1)

int main(void)
{
    ll_irq_init_vector_table();
    ll_rcc_enable_apb2(RCC_APB2ENR_IOPAEN);
    ll_rcc_enable_apb1(RCC_APB1ENR_USART2EN);
    ll_rcc_enable_ahb(RCC_AHBENR_DMA1EN);
    ll_gpio_config_pin(COM_RX_PIN, LL_GPIO_PIN_MODE_INPUT_PULLUP);
    ll_irq_register(COM_USART_TX_DMA_IRQn, COM_USART_TX_DMA_IRQ_HANDLER, 0);
    ll_irq_register(COM_USART_IRQn, COM_USART_IRQ_HANDLER, 0);

    USART_TypeDef *usart = COM_USART; //LL_USART_GET_PERIPH(COM_LL_USART_HANDLE);
    usart->BRR = 8000000 / 9600;
    usart->CR1 = USART_CR1_UE | USART_CR1_TE;

    ll_gpio_config_pin(COM_TX_PIN, LL_GPIO_PIN_MODE_FUNCTION_PP_MSPEED);

    // test com
    com_put_string("\r\nhello world!");
    COM_PUT_CONST_STRING("\r\nHELLO WORLD!");
    LL_GPIO_WRITE_PIN(COM_TX_PIN, LL_GPIO_READ_PIN(COM_RX_PIN));
    return 0;
}