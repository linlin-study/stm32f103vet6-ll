#ifndef LL_RCC_H
#define LL_RCC_H

#include "ll_common.h"

void ll_rcc_enable_ahb(uint32_t mask);
void ll_rcc_enable_apb1(uint32_t mask);
void ll_rcc_enable_apb2(uint32_t mask);

void ll_rcc_disable_ahb(uint32_t mask);
void ll_rcc_disable_apb1(uint32_t mask);
void ll_rcc_disable_apb2(uint32_t mask);

#endif /* LL_RCC_H */