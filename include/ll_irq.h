#ifndef LL_IRQ_H
#define LL_IRQ_H

#include "ll_common.h"

#define LL_IRQ_PRIORITY(n) (((n) << (8 - __NVIC_PRIO_BITS)) & 0xFFU)

typedef void (*ll_irq_handler_t)(void);

/**
 * @param irqn IRQn_Type
 * @param handler ll_irq_handler_t
 * @param priority LL_IRQ_PRIORITY(n)
 */
void ll_irq_register(int32_t irqn, ll_irq_handler_t handler, uint32_t priority);
void ll_irq_unregister(int32_t irqn);
void ll_irq_init_vector_table(void);

#endif /* LL_IRQ_H */