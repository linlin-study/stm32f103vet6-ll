#ifndef LL_USART_H
#define LL_USART_H

#include "ll_common.h"

/* ll_usart */
extern const struct ll_usart LL_USART1;
extern const struct ll_usart LL_USART2;
extern const struct ll_usart LL_USART3;
extern const struct ll_usart LL_UART4;
extern const struct ll_usart LL_UART5;

typedef const struct ll_usart *ll_usart_handle_t;

#define LL_USART1_HANDLE (&LL_USART1)
#define LL_USART2_HANDLE (&LL_USART2)
#define LL_USART3_HANDLE (&LL_USART3)
#define LL_UART4_HANDLE (&LL_UART4)
#define LL_UART5_HANDLE (&LL_UART5)

#define LL_USART_GET_PERIPH(h) (*(USART_TypeDef **)(h))
const char *ll_usart_get_periph_name(USART_TypeDef *periph);

void USART1_IRQHandler(void);
void USART2_IRQHandler(void);
void USART3_IRQHandler(void);
void UART4_IRQHandler(void);
void UART5_IRQHandler(void);

/*******************************************************************************
 * send
 ******************************************************************************/

#ifndef LL_USART_SEND_BYTES
#define LL_USART_SEND_BYTES 1
#endif

void ll_usart_send_byte_by_polling(ll_usart_handle_t h, uint32_t c);

#if LL_USART_SEND_BYTES
typedef void (*ll_usart_tx_complete_callback_t)(const void *buffer);
void ll_usart_set_tx_complete_callback(ll_usart_handle_t h, ll_usart_tx_complete_callback_t callback);
void ll_usart_send_bytes_by_dma(ll_usart_handle_t h, const void *buffer, uint32_t nbytes);
void ll_usart_send_bytes_by_interrupt(ll_usart_handle_t h, const void *buffer, uint32_t nbytes);
#endif

/*******************************************************************************
 * recv
 ******************************************************************************/

#ifndef LL_USART_RECV_BYTES
#define LL_USART_RECV_BYTES 1
#endif

uint32_t ll_usart_recv_byte_by_polling(ll_usart_handle_t h);

#if LL_USART_RECV_BYTES
typedef void (*ll_usart_rx_indicate_callback_dma_t)(const void *buffer, uint32_t nbytes);
typedef void (*ll_usart_rx_indicate_callback_interrupt_t)(uint32_t c);
void ll_usart_start_recv_by_dma(ll_usart_handle_t h, ll_usart_rx_indicate_callback_dma_t callback, uint32_t *buffer, uint32_t size);
void ll_usart_start_recv_by_interrupt(ll_usart_handle_t h, ll_usart_rx_indicate_callback_interrupt_t callback);
void ll_usart_stop_recv(ll_usart_handle_t h);
#endif

#endif /* LL_USART_H */