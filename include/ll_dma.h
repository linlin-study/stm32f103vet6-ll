#ifndef LL_DMA_H
#define LL_DMA_H

#include "ll_common.h"

/* ll_dma_channel */
extern const struct ll_dma_channel LL_DMA1_CHANNEL1;
extern const struct ll_dma_channel LL_DMA1_CHANNEL2;
extern const struct ll_dma_channel LL_DMA1_CHANNEL3;
extern const struct ll_dma_channel LL_DMA1_CHANNEL4;
extern const struct ll_dma_channel LL_DMA1_CHANNEL5;
extern const struct ll_dma_channel LL_DMA1_CHANNEL6;
extern const struct ll_dma_channel LL_DMA1_CHANNEL7;

extern const struct ll_dma_channel LL_DMA2_CHANNEL1;
extern const struct ll_dma_channel LL_DMA2_CHANNEL2;
extern const struct ll_dma_channel LL_DMA2_CHANNEL3;
extern const struct ll_dma_channel LL_DMA2_CHANNEL4;
extern const struct ll_dma_channel LL_DMA2_CHANNEL5;

typedef const struct ll_dma_channel *ll_dma_channel_handle_t;

#define LL_DMA1_CHANNEL1_HANDLE (&LL_DMA1_CHANNEL1)
#define LL_DMA1_CHANNEL2_HANDLE (&LL_DMA1_CHANNEL2)
#define LL_DMA1_CHANNEL3_HANDLE (&LL_DMA1_CHANNEL3)
#define LL_DMA1_CHANNEL4_HANDLE (&LL_DMA1_CHANNEL4)
#define LL_DMA1_CHANNEL5_HANDLE (&LL_DMA1_CHANNEL5)
#define LL_DMA1_CHANNEL6_HANDLE (&LL_DMA1_CHANNEL6)
#define LL_DMA1_CHANNEL7_HANDLE (&LL_DMA1_CHANNEL7)

#define LL_DMA2_CHANNEL1_HANDLE (&LL_DMA2_CHANNEL1)
#define LL_DMA2_CHANNEL2_HANDLE (&LL_DMA2_CHANNEL2)
#define LL_DMA2_CHANNEL3_HANDLE (&LL_DMA2_CHANNEL3)
#define LL_DMA2_CHANNEL4_HANDLE (&LL_DMA2_CHANNEL4)
#define LL_DMA2_CHANNEL5_HANDLE (&LL_DMA2_CHANNEL5)

#define LL_DMA_CHANNEL_GET_PERIPH(h) (*(DMA_Channel_TypeDef **)(h))
const char *ll_dma_get_periph_name(DMA_Channel_TypeDef *periph);

void DMA1_Channel1_IRQHandler(void);
void DMA1_Channel2_IRQHandler(void);
void DMA1_Channel3_IRQHandler(void);
void DMA1_Channel4_IRQHandler(void);
void DMA1_Channel5_IRQHandler(void);
void DMA1_Channel6_IRQHandler(void);
void DMA1_Channel7_IRQHandler(void);

void DMA2_Channel1_IRQHandler(void);
void DMA2_Channel2_IRQHandler(void);
void DMA2_Channel3_IRQHandler(void);
void DMA2_Channel4_5_IRQHandler(void);

#ifndef DMA2_Channel4_IRQHandler
#define DMA2_Channel4_IRQHandler DMA2_Channel4_5_IRQHandler
#endif

#ifndef DMA2_Channel5_IRQHandler
#define DMA2_Channel5_IRQHandler DMA2_Channel4_5_IRQHandler
#endif

/* LL_DMA_CCR_PL */
#define LL_DMA_CCR_PL_LOW (0x0UL << DMA_CCR_PL_Pos)
#define LL_DMA_CCR_PL_MEDIUM (0x1UL << DMA_CCR_PL_Pos)
#define LL_DMA_CCR_PL_HIGH (0x2UL << DMA_CCR_PL_Pos)
#define LL_DMA_CCR_PL_VERY_HIGH (0x3UL << DMA_CCR_PL_Pos)

/* LL_DMA_CCR_MSIZE */
#define LL_DMA_CCR_MSIZE_8BITS (0x0UL << DMA_CCR_MSIZE_Pos)
#define LL_DMA_CCR_MSIZE_16BITS (0x1UL << DMA_CCR_MSIZE_Pos)
#define LL_DMA_CCR_MSIZE_32BITS (0x2UL << DMA_CCR_MSIZE_Pos)

/* LL_DMA_CCR_PSIZE */
#define LL_DMA_CCR_PSIZE_8BITS (0x0UL << DMA_CCR_PSIZE_Pos)
#define LL_DMA_CCR_PSIZE_16BITS (0x1UL << DMA_CCR_PSIZE_Pos)
#define LL_DMA_CCR_PSIZE_32BITS (0x2UL << DMA_CCR_PSIZE_Pos)

/* LL_DMA_CCR_DIR */
#define LL_DMA_CCR_DIR_READ_FROM_PERIPH (0x0UL << DMA_CCR_DIR_Pos)
#define LL_DMA_CCR_DIR_READ_FROM_MEMORY (0x1UL << DMA_CCR_DIR_Pos)

/* LL_DMA_CCR_MINC */
#define LL_DMA_CCR_MINC_ENABLE DMA_CCR_MINC
#define LL_DMA_CCR_MINC_DISABLE 0

/* LL_DMA_CCR_PINC */
#define LL_DMA_CCR_PINC_ENABLE DMA_CCR_PINC
#define LL_DMA_CCR_PINC_DISABLE 0

/* LL_DMA_CCR_CIRC */
#define LL_DMA_CCR_CIRC_ENABLE DMA_CCR_CIRC
#define LL_DMA_CCR_CIRC_DISABLE 0

/* LL_DMA_MEM2MEM */
#define LL_DMA_CCR_MEM2MEM_ENABLE DMA_CCR_MEM2MEM
#define LL_DMA_CCR_MEM2MEM_DISABLE 0

/* LL_DMA_CCR_TEIE */
#define LL_DMA_CCR_TEIE_ENABLE DMA_CCR_TEIE
#define LL_DMA_CCR_TEIE_DISABLE 0

/* LL_DMA_CCR_TCIE */
#define LL_DMA_CCR_TCIE_ENABLE DMA_CCR_TCIE
#define LL_DMA_CCR_TCIE_DISABLE 0

/* LL_DMA_CCR_HTIE */
#define DMA_CCR_HTIE_ENABLE DMA_CCR_HTIE
#define DMA_CCR_HTIE_DISABLE 0

/* LL_DMA_ISR_FLAG */
#define LL_DMA_ISR_FLAG_TE DMA_ISR_TEIF1
#define LL_DMA_ISR_FLAG_TC DMA_ISR_TCIF1
#define LL_DMA_ISR_FLAG_HT DMA_ISR_HTIF1

/**
 * @param flga LL_DMA_ISR_FLAG
 */
typedef void (*ll_dma_isr_callback_t)(uint32_t flag);

/**
 * @param h 实例句柄
 * @param callback 中断回调函数
 * @param cpar 外设地址
 * @param cmar 内存地址
 * @param ccr 控制字
 */
void ll_dma_start(ll_dma_channel_handle_t h, ll_dma_isr_callback_t callback, uint32_t cpar, uint32_t cmar, uint32_t cndtr, uint32_t ccr);

#endif /* LL_DMA_H */