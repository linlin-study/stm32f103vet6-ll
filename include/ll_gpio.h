#ifndef LL_GPIO_H
#define LL_GPIO_H

#include "ll_common.h"

extern GPIO_TypeDef *const LL_GPIO_TABLE[];

#define LL_GPIO_PIN_DEFINE(PORT, PINN) ((((PORT) << 4) | (PINN)) + BUILD_BUG_OR_ZERO((PINN) > 15U))
#define LL_GPIO_PIN_PORT(PIN) (((PIN) >> 4) & (0xFU))
#define LL_GPIO_PIN_PINN(PIN) ((PIN) & (0xFU))
#define LL_GPIO_PIN_NULL 0xFFU

enum ll_gpio_pin_mode
{
    LL_GPIO_PIN_MODE_INPUT_FLOAT,
    LL_GPIO_PIN_MODE_INPUT_PULLUP,
    LL_GPIO_PIN_MODE_INPUT_PULLDOWN,

    LL_GPIO_PIN_MODE_OUTPUT_PP_LSPEED_LOW,
    LL_GPIO_PIN_MODE_OUTPUT_PP_MSPEED_LOW,
    LL_GPIO_PIN_MODE_OUTPUT_PP_HSPEED_LOW,

    LL_GPIO_PIN_MODE_OUTPUT_OD_LSPEED_LOW,
    LL_GPIO_PIN_MODE_OUTPUT_OD_MSPEED_LOW,
    LL_GPIO_PIN_MODE_OUTPUT_OD_HSPEED_LOW,

    LL_GPIO_PIN_MODE_OUTPUT_PP_LSPEED_HIGH,
    LL_GPIO_PIN_MODE_OUTPUT_PP_MSPEED_HIGH,
    LL_GPIO_PIN_MODE_OUTPUT_PP_HSPEED_HIGH,

    LL_GPIO_PIN_MODE_OUTPUT_OD_LSPEED_HIGH,
    LL_GPIO_PIN_MODE_OUTPUT_OD_MSPEED_HIGH,
    LL_GPIO_PIN_MODE_OUTPUT_OD_HSPEED_HIGH,

    LL_GPIO_PIN_MODE_FUNCTION_PP_LSPEED,
    LL_GPIO_PIN_MODE_FUNCTION_PP_MSPEED,
    LL_GPIO_PIN_MODE_FUNCTION_PP_HSPEED,

    LL_GPIO_PIN_MODE_FUNCTION_OD_LSPEED,
    LL_GPIO_PIN_MODE_FUNCTION_OD_MSPEED,
    LL_GPIO_PIN_MODE_FUNCTION_OD_HSPEED,

    LL_GPIO_PIN_MODE_ANALOG,
};

/**
 * 配置引脚
 * @param pin LL_GPIO_PIN_DEFINE(port, pin)
 * @param mode enum ll_gpio_pin_mode
 */
void ll_gpio_config_pin(uint32_t pin, uint32_t mode);

#define LL_GPIO_PIN_LOCK BIT(31)

/**
 * 写引脚
 * @param pin LL_GPIO_PIN_DEFINE(port, pin)
 * @param value 0 or!0
 */
void ll_gpio_write_pin(uint32_t pin, uint32_t value);

#define LL_GPIO_WRITE_PIN(PIN, VALUE)                                                \
    do                                                                               \
    {                                                                                \
        if (VALUE)                                                                   \
        {                                                                            \
            LL_GPIO_TABLE[LL_GPIO_PIN_PORT(PIN)]->BSRR = BIT(LL_GPIO_PIN_PINN(PIN)); \
        }                                                                            \
        else                                                                         \
        {                                                                            \
            LL_GPIO_TABLE[LL_GPIO_PIN_PORT(PIN)]->BRR = BIT(LL_GPIO_PIN_PINN(PIN));  \
        }                                                                            \
    } while (0)

/**
 * 读引脚
 * @param pin LL_GPIO_PIN_DEFINE(port, pin)
 * @return 0 or!0
 */
uint32_t ll_gpio_read_pin(uint32_t pin);

#define LL_GPIO_READ_PIN(PIN) (LL_GPIO_TABLE[LL_GPIO_PIN_PORT(PIN)]->IDR & BIT(LL_GPIO_PIN_PINN(PIN)))

/**
 * 配置重映射
 * @param mask AFIO_MAPR
 */
void ll_afio_remap(uint32_t mask);

#ifndef LL_AFIO_MAPR_SWJ_CFG
#define LL_AFIO_MAPR_SWJ_CFG AFIO_MAPR_SWJ_CFG_JTAGDISABLE
#endif

#endif /* LL_GPIO_H */