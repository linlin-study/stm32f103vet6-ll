#ifndef LL_COMMON_H
#define LL_COMMON_H

/* cmsis */
#ifndef STM32F103xE
#define STM32F103xE
#endif
#include "stm32f1xx.h"

/* stdlib */
#include <stdint.h>

/* IAR Compiler */
#ifndef __AT
#define __AT(x) @x
#endif

#ifndef __SECTION
#define __SECTION(x) __attribute__((section(x)))
#endif

#if defined(__ICCARM__)
#ifndef __NO_INIT
#define __NO_INIT __no_init
#endif

#undef __USED /* undo define in cmsis_iccarm.h */
#ifndef __USED
#define __USED __root
#endif

#ifndef STATIC_ASSERT
#define STATIC_ASSERT static_assert
#endif

/* GNU Compiler */
#elif defined(__GNUC__)

#ifndef __AT
#define __AT(x)
#endif

#ifndef __SECTION
#define __SECTION(x) __attribute__((section(x)))
#endif

#ifndef __NO_INIT
#define __NO_INIT __SECTION(".noinit")
#endif

#ifndef STATIC_ASSERT
#define STATIC_ASSERT _Static_assert
#endif

/* Unknown Compiler */
#else
#warning Unknown compiler.
#endif

/* BUILD_BUG */
#define BUILD_BUG_OR_ZERO(e) (sizeof(char[1 - 2 * !!(e)]) - 1)
#define BUILD_BUG_ON_MSG(cond, msg) STATIC_ASSERT(!(cond), msg)
#define BUILD_BUG_ON(cond) BUILD_BUG_ON_MSG(cond, "BUILD_BUG_ON: " #cond)

/* LOG: DEBUG < INFO < WARN < ERROR < FATAL */
#ifndef LOG
#include <stdio.h>
#define LOG(...) printf("\r\n"__VA_ARGS__)
#endif

#ifndef LOG_DEBUG
#define LOG_DEBUG(...) LOG("[DEBUG]"__VA_ARGS__)
#endif

#ifndef LOG_INFO
#define LOG_INFO(...) LOG("[INFO]"__VA_ARGS__)
#endif

#ifndef LOG_WARN
#define LOG_WARN(...) LOG("[WARN]"__VA_ARGS__)
#endif

#ifndef LOG_ERROR
#define LOG_ERROR(...) LOG("[ERROR]"__VA_ARGS__)
#endif

#ifndef LOG_FATAL
#define LOG_FATAL(...) LOG("[FATAL]"__VA_ARGS__)
#endif

/* ASSERT & VERIFY */
#ifndef ASSERT
#ifndef NDEBUG
#include <stdlib.h>
#define ASSERT(x)                                                                 \
    do                                                                            \
    {                                                                             \
        if ((x) == 0)                                                             \
        {                                                                         \
            LOG_DEBUG("AssertFailed: %s, in %s line %d", #x, __FILE__, __LINE__); \
            abort();                                                              \
        }                                                                         \
    } while (0)
#else
#define ASSERT(x) ((void)0)
#endif
#endif

#ifndef NDEBUG
#define VERIFY(x) ASSERT(x)
#else
#define VERIFY(x) (x)
#endif

/* REPEAT_BYTE - repeat the value @x multiple times to ulong */
#define REPEAT_BYTE(x) ((~0UL / 0xFF) * (x))
#define REPEAT_NIBBLE(x) ((~0UL / 0x0F) * (x))

/* cast a member of a structure out to the containing structure */
#define container_of(ptr, type, member) ((type *)((char *)(ptr)-offsetof(type, member)))

/**
 * ARRAY_SIZE - get the number of elements in array @arr
 * @arr: array to be sized
 */
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof(arr[0]))

/* power of two */
#define is_power_of_two(n) (((n) & ((n)-1)) == 0)
#define rounddown_power_of_two(size) ((~(SIZE_MAX >> 1)) >> __CLZ(size))
#define roundup_power_of_two(size) (is_power_of_two(size) ? size : (rounddown_power_of_two(size) << 1))

/* align addr to size, size must be power of 2 */
#define ALIGN_(addr, size) (((uint32_t)(addr) + (size) - (1)) & (~((size) - (1)))) /* ALIGN(13, 4) = 16 */
#define ALIGN_DOWN_(addr, size) ((uint32_t)(addr) & (~((size) - (1))))             /* ALIGN_DOWN(13, 4) = 12 */
#define IS_ALIGNED_(addr, size) (((uint32_t)(addr) & ((size) - (1))) == 0)         /* IS_ALIGNED(13, 4) = false */

#define ALIGN(addr, size) (ALIGN_(addr, size) + BUILD_BUG_OR_ZERO(!is_power_of_two(size)))            /* ALIGN(13, 4) = 16 */
#define ALIGN_DOWN(addr, size) (ALIGN_DOWN_(addr, size) + BUILD_BUG_OR_ZERO(!is_power_of_two(size)))  /* ALIGN_DOWN(13, 4) = 12 */
#define IS_ALIGNED(addr, size) (IS_ALIGNED_(addr, size) || BUILD_BUG_OR_ZERO(!is_power_of_two(size))) /* IS_ALIGNED(13, 4) = false */

/* min, max, clamp */
#define min(x, y) ((x) > (y) ? (y) : (x))
#define max(x, y) ((x) < (y) ? (y) : (x))
#define clamp(val, min, max) ((val) < (min) ? (min) : ((val) > (max) ? (max) : (val)))

/* div round */
#define DIV_ROUND_UP(n, d) (((n) + (d) - (1)) / (d))
#define DIV_ROUND_CLOSEST(n, d) (((n) + (d) / 2) / (d))

#define roundup(x, y) ((((x) + (y) - (1)) / (y)) * (y))
#define rounddown(x, y) ((x) - (x) % (y))

/* REG PTR */
#define REG8(addr) (*(volatile uint8_t *)(addr))
#define REG16(addr) (*(volatile uint16_t *)(addr))
#define REG32(addr) (*(volatile uint32_t *)(addr))

/* BIT(S) MASK */
#define BIT(x) (1UL << (x))
#define BITS(start, end) ((0xFFFFFFFFUL << (start)) & (0xFFFFFFFFUL >> (31 - (end))))

/* ll_fifo */
struct ll_fifo
{
    uint8_t *buffer;
    uint32_t size; // must be power of two
    uint32_t in;
    uint32_t out;
    uint32_t trigger;
    void (*callback)(struct ll_fifo *fifo);
};

#define LL_FIFO_LENGTH(fifo) (fifo->in - fifo->out)

uint32_t ll_fifo_put(struct ll_fifo *fifo, const void *buffer, uint32_t len);
uint32_t ll_fifo_get(struct ll_fifo *fifo, const void *buffer, uint32_t len);

#endif /* LL_COMMON_H */