#include "ll_usart.h"
#include "ll_dma.h"

struct ll_usart_dma
{
    const struct ll_dma_channel *ll_dma_channel;
    ll_dma_isr_callback_t ll_dma_isr_callback;
};

struct ll_usart_buffer
{
    uint8_t *ptr;
    uint32_t len;
    const void *buffer;
};

struct ll_usart
{
    USART_TypeDef *usart;

#if LL_USART_SEND_BYTES
    ll_usart_tx_complete_callback_t *tx_complete_callback_ptr;
    union
    {
        const struct ll_usart_dma *tx_dma;
        struct ll_usart_buffer *tx_buffer;
    };
#endif

#if LL_USART_RECV_BYTES
    void *rx_indicate_callback_ptr;
    const struct ll_usart_dma *rx_dma;
#endif
};

const char *ll_usart_get_periph_name(USART_TypeDef *periph)
{
    if (periph == USART1)
        return "USART1";
    if (periph == USART2)
        return "USART2";
    if (periph == USART3)
        return "USART3";
    if (periph == UART4)
        return "UART4";
    if (periph == UART5)
        return "UART5";
    return "UnknowPeriph";
}

/*******************************************************************************
 * send
 ******************************************************************************/

void ll_usart_send_byte_by_polling(ll_usart_handle_t h, uint32_t c)
{
    USART_TypeDef *usart = h->usart;

#if LL_USART_SEND_BYTES
    uint32_t cr1 = usart->CR1;
    //if (cr1 & USART_CR1_TXEIE)
    {
        usart->CR1 = cr1 & (~USART_CR1_TXEIE);
    }
    uint32_t cr3 = usart->CR3;
    //if (cr3 & USART_CR3_DMAT)
    {
        usart->CR3 = cr3 & (~USART_CR3_DMAT);
    }
#endif

    while ((usart->SR & USART_SR_TXE) == 0)
        ;
    usart->DR = c;

#if LL_USART_SEND_BYTES
    //if (cr1 & USART_CR1_TXEIE)
    {
        usart->CR1 = cr1;
    }
    //if (cr3 & USART_CR3_DMAT)
    {
        usart->CR3 = cr3;
    }
#endif
}

#if LL_USART_SEND_BYTES

void ll_usart_set_tx_complete_callback(ll_usart_handle_t h, ll_usart_tx_complete_callback_t callback)
{
    *h->tx_complete_callback_ptr = callback;
}

void ll_usart_send_bytes_by_interrupt(ll_usart_handle_t h, const void *buffer, uint32_t nbytes)
{
    ASSERT(nbytes);
    ASSERT((uint32_t)(h->tx_buffer) > SRAM_BASE);
    uint8_t *ptr = (uint8_t *)buffer;
    USART_TypeDef *usart = h->usart;
    if (usart->SR & USART_SR_TXE)
    {
        usart->DR = *ptr++;
    }
    if ((uint32_t)ptr < ((uint32_t)buffer + nbytes))
    {
        struct ll_usart_buffer *tx_buffer = h->tx_buffer;
        tx_buffer->buffer = buffer;
        tx_buffer->len = nbytes;
        tx_buffer->ptr = ptr;
        usart->CR1 |= USART_CR1_TXEIE;
    }
}

static inline void ll_usart_tx_isr(ll_usart_handle_t h, uint32_t sr)
{
    USART_TypeDef *usart = h->usart;
    if (sr & USART_SR_TXE)
    {
        uint32_t cr1 = usart->CR1;
        if (cr1 & USART_CR1_TXEIE)
        {
            struct ll_usart_buffer *tx_buffer = h->tx_buffer;
            uint32_t base = (uint32_t)(tx_buffer->buffer);
            uint32_t len = tx_buffer->len;
            uint8_t *ptr = tx_buffer->ptr;
            uint8_t *end = (uint8_t *)(base + len);
            if (ptr < end)
            {
                usart->DR = *ptr++;
                if (ptr == end)
                {
                    usart->CR1 = cr1 & (~USART_CR1_TXEIE);
                    ll_usart_tx_complete_callback_t callback = *h->tx_complete_callback_ptr;
                    if (callback)
                    {
                        callback((void *)base);
                    }
                }
                else
                {
                    tx_buffer->ptr = ptr;
                }
            }
        }
    }
}

void ll_usart_send_bytes_by_dma(ll_usart_handle_t h, const void *buffer, uint32_t nbytes)
{
    ASSERT(nbytes);
    ASSERT((uint32_t)(h->tx_dma) < SRAM_BASE);
    ASSERT((uint32_t)(h->tx_dma) > FLASH_BASE);
    USART_TypeDef *usart = h->usart;
    const struct ll_usart_dma *tx_dma = h->tx_dma;
    ll_dma_start(tx_dma->ll_dma_channel, tx_dma->ll_dma_isr_callback,
                 (uint32_t)(&(usart->DR)), (uint32_t)buffer, nbytes,
                 DMA_CCR_EN |
                     LL_DMA_CCR_PL_LOW |
                     LL_DMA_CCR_DIR_READ_FROM_MEMORY |
                     LL_DMA_CCR_MSIZE_8BITS |
                     LL_DMA_CCR_PSIZE_8BITS |
                     LL_DMA_CCR_MINC_ENABLE |
                     LL_DMA_CCR_TEIE_ENABLE |
                     LL_DMA_CCR_TCIE_ENABLE);
    usart->CR3 |= USART_CR3_DMAT;
}

static inline void ll_usart_tx_dma_isr_callback(uint32_t flag, ll_usart_handle_t h /*, DMA_Channel_TypeDef *dmach*/)
{
    if (flag & LL_DMA_ISR_FLAG_TC)
    {
        USART_TypeDef *usart = h->usart;
        ll_usart_tx_complete_callback_t callback = *h->tx_complete_callback_ptr;
        usart->CR3 &= ~USART_CR3_DMAT;
        if (callback)
        {
            DMA_Channel_TypeDef *dmach = LL_DMA_CHANNEL_GET_PERIPH(h->tx_dma->ll_dma_channel);
            callback((void *)(dmach->CMAR));
        }
    }
}

#define LL_USART_TX_DMA_ISR_CALLBACK(USART) \
    ll_usart_tx_dma_isr_callback(flag, LL_##USART##_HANDLE /*, LL_DMA_CHANNEL_GET_PERIPH(LL_##USART##_TX_DMA.ll_dma_channel)*/)

static void ll_usart1_tx_dma_isr_callback(uint32_t flag);
static void ll_usart2_tx_dma_isr_callback(uint32_t flag);
static void ll_usart3_tx_dma_isr_callback(uint32_t flag);
static void ll_uart4_tx_dma_isr_callback(uint32_t flag);
static const struct ll_usart_dma LL_USART1_TX_DMA = {&LL_DMA1_CHANNEL4, ll_usart1_tx_dma_isr_callback};
static const struct ll_usart_dma LL_USART2_TX_DMA = {&LL_DMA1_CHANNEL7, ll_usart2_tx_dma_isr_callback};
static const struct ll_usart_dma LL_USART3_TX_DMA = {&LL_DMA1_CHANNEL2, ll_usart3_tx_dma_isr_callback};
static const struct ll_usart_dma LL_UART4_TX_DMA = {&LL_DMA2_CHANNEL5, ll_uart4_tx_dma_isr_callback};

static void ll_usart1_tx_dma_isr_callback(uint32_t flag)
{
    LL_USART_TX_DMA_ISR_CALLBACK(USART1);
}
static void ll_usart2_tx_dma_isr_callback(uint32_t flag)
{
    LL_USART_TX_DMA_ISR_CALLBACK(USART2);
}
static void ll_usart3_tx_dma_isr_callback(uint32_t flag)
{
    LL_USART_TX_DMA_ISR_CALLBACK(USART3);
}
static void ll_uart4_tx_dma_isr_callback(uint32_t flag)
{
    LL_USART_TX_DMA_ISR_CALLBACK(UART4);
}

__NO_INIT struct ll_usart_buffer ll_usart1_tx_buffer;
__NO_INIT struct ll_usart_buffer ll_usart2_tx_buffer;
__NO_INIT struct ll_usart_buffer ll_usart3_tx_buffer;
__NO_INIT struct ll_usart_buffer ll_uart4_tx_buffer;
__NO_INIT struct ll_usart_buffer ll_uart5_tx_buffer;

static ll_usart_tx_complete_callback_t ll_usart1_tx_complete_callback = 0;
static ll_usart_tx_complete_callback_t ll_usart2_tx_complete_callback = 0;
static ll_usart_tx_complete_callback_t ll_usart3_tx_complete_callback = 0;
static ll_usart_tx_complete_callback_t ll_uart4_tx_complete_callback = 0;
static ll_usart_tx_complete_callback_t ll_uart5_tx_complete_callback = 0;

#endif

/*******************************************************************************
 * recv
 ******************************************************************************/

uint32_t ll_usart_recv_byte_by_polling(ll_usart_handle_t h)
{
    USART_TypeDef *usart = h->usart;

#if LL_USART_RECV_BYTES
    uint32_t cr1 = usart->CR1;
    //if (cr1 & USART_CR1_RXNEIE)
    {
        usart->CR1 = cr1 & (~USART_CR1_RXNEIE);
    }
    uint32_t cr3 = usart->CR3;
    //if (cr3 & USART_CR3_DMAR)
    {
        usart->CR3 = cr3 & (~USART_CR3_DMAR);
    }
#endif

    while ((usart->SR & USART_SR_RXNE) == 0)
        ;
    uint32_t dr = usart->DR;

#if LL_USART_RECV_BYTES
    //if (cr1 & USART_CR1_RXNEIE)
    {
        usart->CR1 = cr1;
    }
    //if (cr3 & USART_CR3_DMAR)
    {
        usart->CR3 = cr3;
    }
#endif

    return dr;
}

#if LL_USART_RECV_BYTES

void ll_usart_stop_recv(ll_usart_handle_t h)
{
    USART_TypeDef *usart = h->usart;
    usart->CR3 &= ~USART_CR3_DMAR;
    usart->CR1 &= ~(USART_CR1_IDLEIE | USART_CR1_RXNEIE);
}

void ll_usart_start_recv_by_dma(ll_usart_handle_t h, ll_usart_rx_indicate_callback_dma_t callback, uint32_t *buffer, uint32_t size)
{
    ASSERT(size > 4);
    ASSERT((uint32_t)(h->rx_dma) < SRAM_BASE);
    ASSERT((uint32_t)(h->rx_dma) > FLASH_BASE);

    *((ll_usart_rx_indicate_callback_dma_t *)h->rx_indicate_callback_ptr) = callback;
    size -= 4;
    *buffer++ = size;

    USART_TypeDef *usart = h->usart;
    const struct ll_usart_dma *rx_dma = h->rx_dma;

    ll_dma_start(rx_dma->ll_dma_channel, rx_dma->ll_dma_isr_callback,
                 (uint32_t)(&(usart->DR)), (uint32_t)buffer, size,
                 DMA_CCR_EN |
                     LL_DMA_CCR_PL_HIGH |
                     LL_DMA_CCR_DIR_READ_FROM_PERIPH |
                     LL_DMA_CCR_MSIZE_8BITS |
                     LL_DMA_CCR_PSIZE_8BITS |
                     LL_DMA_CCR_MINC_ENABLE |
                     LL_DMA_CCR_CIRC_ENABLE |
                     LL_DMA_CCR_TEIE_ENABLE |
                     LL_DMA_CCR_TCIE_ENABLE);
    usart->CR1 |= USART_CR1_IDLEIE;
    usart->CR3 |= USART_CR3_DMAR;
}

static inline void ll_usart_rx_dma_isr_callback(uint32_t flag, ll_usart_handle_t h)
{
    if (flag & LL_DMA_ISR_FLAG_TC)
    {
        ll_usart_rx_indicate_callback_dma_t callback = *((ll_usart_rx_indicate_callback_dma_t *)(h->rx_indicate_callback_ptr));
        if (callback)
        {
            DMA_Channel_TypeDef *dmach = LL_DMA_CHANNEL_GET_PERIPH(h->rx_dma->ll_dma_channel);
            uint32_t addr = dmach->CMAR;
            uint32_t size = *((uint32_t *)(addr - 4));
            callback((void *)addr, size);
        }
    }
}

#define LL_USART_RX_DMA_ISR_CALLBACK(USART) \
    ll_usart_rx_dma_isr_callback(flag, LL_##USART##_HANDLE)

static inline void ll_usart_rx_isr_idle(ll_usart_handle_t h)
{
    ll_usart_rx_indicate_callback_dma_t callback = *((ll_usart_rx_indicate_callback_dma_t *)(h->rx_indicate_callback_ptr));
    if (callback)
    {
        DMA_Channel_TypeDef *dmach = LL_DMA_CHANNEL_GET_PERIPH(h->rx_dma->ll_dma_channel);
        uint32_t addr = dmach->CMAR;
        uint32_t size = *((uint32_t *)(addr - 4));
        uint32_t cndt = dmach->CNDTR;
        if (cndt < size)
        {
            callback((void *)addr, size - cndt);
        }
    }
}

void ll_usart_start_recv_by_interrupt(ll_usart_handle_t h, ll_usart_rx_indicate_callback_interrupt_t callback)
{
    *((ll_usart_rx_indicate_callback_interrupt_t *)(h->rx_indicate_callback_ptr)) = callback;
    h->usart->CR1 |= USART_CR1_RXNEIE;
}

static inline uint32_t ll_usart_rx_isr(ll_usart_handle_t h)
{
    USART_TypeDef *usart = h->usart;
    uint32_t sr = usart->SR;
    uint8_t dr = usart->DR;

    if (sr & (USART_SR_PE | USART_SR_NE | USART_SR_FE))
    {
        LOG_INFO("%s recv error: 0x%lX", ll_usart_get_periph_name(h->usart), sr);
    }
    else
    {
        uint32_t cr1 = usart->CR1;
        if ((sr & USART_SR_IDLE) && (cr1 & USART_CR1_IDLEIE))
        {
            ll_usart_rx_isr_idle(h);
        }
        else if (sr & USART_SR_RXNE)
        {
            ll_usart_rx_indicate_callback_interrupt_t callback = *((ll_usart_rx_indicate_callback_interrupt_t *)(h->rx_indicate_callback_ptr));
            if (callback)
            {
                callback(dr);
            }
        }
    }

    return sr;
}

static void ll_usart1_rx_dma_isr_callback(uint32_t flag);
static void ll_usart2_rx_dma_isr_callback(uint32_t flag);
static void ll_usart3_rx_dma_isr_callback(uint32_t flag);
static void ll_uart4_rx_dma_isr_callback(uint32_t flag);
static const struct ll_usart_dma LL_USART1_RX_DMA = {&LL_DMA1_CHANNEL5, ll_usart1_rx_dma_isr_callback};
static const struct ll_usart_dma LL_USART2_RX_DMA = {&LL_DMA1_CHANNEL6, ll_usart2_rx_dma_isr_callback};
static const struct ll_usart_dma LL_USART3_RX_DMA = {&LL_DMA1_CHANNEL3, ll_usart3_rx_dma_isr_callback};
static const struct ll_usart_dma LL_UART4_RX_DMA = {&LL_DMA2_CHANNEL3, ll_uart4_rx_dma_isr_callback};

static void ll_usart1_rx_dma_isr_callback(uint32_t flag)
{
    LL_USART_RX_DMA_ISR_CALLBACK(USART1);
}
static void ll_usart2_rx_dma_isr_callback(uint32_t flag)
{
    LL_USART_RX_DMA_ISR_CALLBACK(USART2);
}
static void ll_usart3_rx_dma_isr_callback(uint32_t flag)
{
    LL_USART_RX_DMA_ISR_CALLBACK(USART3);
}
static void ll_uart4_rx_dma_isr_callback(uint32_t flag)
{
    LL_USART_RX_DMA_ISR_CALLBACK(UART4);
}

__NO_INIT static void *ll_usart1_rx_indicate_callback;
__NO_INIT static void *ll_usart2_rx_indicate_callback;
__NO_INIT static void *ll_usart3_rx_indicate_callback;
__NO_INIT static void *ll_uart4_rx_indicate_callback;
__NO_INIT static void *ll_uart5_rx_indicate_callback;

#endif

/*******************************************************************************
 * instance
 ******************************************************************************/

/* USART1 */
const struct ll_usart LL_USART1 = {
    .usart = USART1,

#if LL_USART_SEND_BYTES
    .tx_complete_callback_ptr = &ll_usart1_tx_complete_callback,
    .tx_dma = &LL_USART1_TX_DMA, // .tx_buffer = &ll_usart1_tx_buffer,
#endif

#if LL_USART_RECV_BYTES
    .rx_indicate_callback_ptr = &ll_usart1_rx_indicate_callback,
    .rx_dma = &LL_USART1_RX_DMA,
#endif
};

__WEAK void USART1_IRQHandler(void) { ll_usart_rx_isr(LL_USART1_HANDLE); }

/* USART2 */
const struct ll_usart LL_USART2 = {
    .usart = USART2,

#if LL_USART_SEND_BYTES
    .tx_complete_callback_ptr = &ll_usart2_tx_complete_callback,
    .tx_dma = &LL_USART2_TX_DMA, //.tx_buffer = &ll_usart2_tx_buffer,
#endif

#if LL_USART_RECV_BYTES
    .rx_indicate_callback_ptr = &ll_usart2_rx_indicate_callback,
    .rx_dma = &LL_USART2_RX_DMA, //.rx_buffer = &LL_USART2_RX_DMA,
#endif
};

__WEAK void USART2_IRQHandler(void) { ll_usart_rx_isr(LL_USART2_HANDLE); }

/* USART3 */
const struct ll_usart LL_USART3 = {
    .usart = USART3,

#if LL_USART_SEND_BYTES
    .tx_complete_callback_ptr = &ll_usart3_tx_complete_callback,
    .tx_dma = &LL_USART3_TX_DMA, //.tx_buffer = &ll_usart3_tx_buffer,
#endif

#if LL_USART_RECV_BYTES
    .rx_indicate_callback_ptr = &ll_usart3_rx_indicate_callback,
    .rx_dma = &LL_USART3_RX_DMA,
#endif
};

__WEAK void USART3_IRQHandler(void) { ll_usart_rx_isr(LL_USART3_HANDLE); }

/* UART4 */
const struct ll_usart LL_UART4 = {
    .usart = UART4,

#if LL_USART_SEND_BYTES
    .tx_complete_callback_ptr = &ll_uart4_tx_complete_callback,
    .tx_dma = &LL_UART4_TX_DMA,
#endif

#if LL_USART_RECV_BYTES
    .rx_indicate_callback_ptr = &ll_uart4_rx_indicate_callback,
    .rx_dma = &LL_UART4_RX_DMA,
#endif
};

__WEAK void UART4_IRQHandler(void) { ll_usart_rx_isr(LL_UART4_HANDLE); }

/* UART5 */
const struct ll_usart LL_UART5 = {
    .usart = UART5,

#if LL_USART_SEND_BYTES
    .tx_complete_callback_ptr = &ll_uart5_tx_complete_callback,
    .tx_buffer = &ll_uart5_tx_buffer,
#endif

#if LL_USART_RECV_BYTES
    .rx_indicate_callback_ptr = &ll_uart5_rx_indicate_callback,
    .rx_dma = 0,
#endif
};

__WEAK void UART5_IRQHandler(void) { ll_usart_tx_isr(LL_UART5_HANDLE, ll_usart_rx_isr(LL_UART5_HANDLE)); }