#include "ll_irq.h"

__NO_INIT static ll_irq_handler_t ll_irq_vector_table[80] __ALIGNED(0x200); // This value must be a multiple of 0x200.

static void ll_default_irq_handler(void)
{
    LOG_ERROR("DefaultIrqHandler: 0x%08lX", __get_xPSR());
    abort();
}

void ll_irq_init_vector_table(void)
{
    BUILD_BUG_ON(ARRAY_SIZE(ll_irq_vector_table) < 2U);

    ll_irq_handler_t *vtor = (ll_irq_handler_t *)(SCB->VTOR);
    for (uint32_t i = ARRAY_SIZE(ll_irq_vector_table) - 1;; --i)
    {
        if (i < 16)
        {
            ll_irq_vector_table[i] = vtor[i];
        }
        else
        {
            ll_irq_vector_table[i] = ll_default_irq_handler;
        }
        if (i == 0) // 2
        {
            break;
        }
    }

    __DSB();
    SCB->VTOR = (uint32_t)ll_irq_vector_table;
}

void ll_irq_register(int32_t irqn, ll_irq_handler_t handler, uint32_t priority)
{
    ASSERT(irqn >= 0);
    uint32_t vector_index = irqn + 16;
    uint32_t iser_index = (uint32_t)irqn >> 5;  // irqn / 32
    uint32_t iser_mask = 1UL << (irqn & 0x1FU); // 1 << (irqn % 32)

    NVIC->ICER[iser_index] = iser_mask; // disable irqn
    __DSB();
    ll_irq_vector_table[vector_index] = handler;
    NVIC->IP[irqn] = priority;
    __DSB();
    NVIC->ISER[iser_index] = iser_mask; // enable irqn
}

void ll_irq_unregister(int32_t irqn)
{
    ASSERT(irqn >= 0);
    uint32_t vector_index = irqn + 16;
    uint32_t iser_index = (uint32_t)irqn >> 5;  // irqn / 32
    uint32_t iser_mask = 1UL << (irqn & 0x1FU); // 1 << (irqn % 32)

    NVIC->ICER[iser_index] = iser_mask; // disable irqn
    __DSB();
    ll_irq_vector_table[vector_index] = ll_default_irq_handler;
    NVIC->IP[irqn] = 0;
}