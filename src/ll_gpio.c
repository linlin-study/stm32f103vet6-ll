#include "ll_gpio.h"

GPIO_TypeDef *const LL_GPIO_TABLE[] = {GPIOA, GPIOB, GPIOC, GPIOD, GPIOE};

/* LL_GPIO_CR_MODE */
#define LL_GPIO_CR_MODER_ANALOG 0x0UL
#define LL_GPIO_CR_MODER_INPUT_FLOAT 0x4UL
#define LL_GPIO_CR_MODER_INPUT_PULL 0x8UL
#define LL_GPIO_CR_MODER_OUTPUT_PP_LSPEED 0x2UL
#define LL_GPIO_CR_MODER_OUTPUT_PP_MSPEED 0x1UL
#define LL_GPIO_CR_MODER_OUTPUT_PP_HSPEED 0x3UL
#define LL_GPIO_CR_MODER_OUTPUT_OD_LSPEED 0x6UL
#define LL_GPIO_CR_MODER_OUTPUT_OD_MSPEED 0x5UL
#define LL_GPIO_CR_MODER_OUTPUT_OD_HSPEED 0x7UL
#define LL_GPIO_CR_MODER_AF_PP_LSPEED 0xAUL
#define LL_GPIO_CR_MODER_AF_PP_MSPEED 0x9UL
#define LL_GPIO_CR_MODER_AF_PP_HSPEED 0xBUL
#define LL_GPIO_CR_MODER_AF_OD_LSPEED 0xEUL
#define LL_GPIO_CR_MODER_AF_OD_MSPEED 0xDUL
#define LL_GPIO_CR_MODER_AF_OD_HSPEED 0xFUL

static inline void ll_gpio_set_cr(GPIO_TypeDef *gpio, uint32_t pinn, uint32_t mode)
{
    uint32_t *cr;
    uint32_t pos;
    if (pinn < 8)
    {
        pos = (pinn - 0) * 4;
        cr = (uint32_t *)&(gpio->CRL);
    }
    else
    {
        pos = (pinn - 8) * 4;
        cr = (uint32_t *)&(gpio->CRH);
    }
    *cr = ((*cr) & ~(0xFUL << pos)) | (mode << pos);
}

static inline void ll_gpio_lock_pin(GPIO_TypeDef *gpio, uint32_t mask)
{
    uint32_t lckr = gpio->LCKR | GPIO_LCKR_LCKK;
    gpio->LCKR = lckr;                   // write 1
    gpio->LCKR = lckr & ~GPIO_LCKR_LCKK; // write 0
    gpio->LCKR = lckr;                   // write 1
    gpio->LCKR = lckr | mask;            // lock
    ASSERT(gpio->LCKR & mask);
}

void ll_gpio_config_pin(uint32_t pin, uint32_t mode)
{
    uint32_t port = LL_GPIO_PIN_PORT(pin);
    ASSERT(port < ARRAY_SIZE(LL_GPIO_TABLE));
    GPIO_TypeDef *gpio = LL_GPIO_TABLE[port];

    uint32_t pinn = LL_GPIO_PIN_PINN(pin);
    uint32_t mask = BIT(pinn);

    if (gpio->LCKR & mask)
    {
        LOG_WARN("pin-%lu-%lu: has been locked", port, pinn);
        return;
    }

    switch (mode)
    {
    case LL_GPIO_PIN_MODE_INPUT_PULLDOWN:
    case LL_GPIO_PIN_MODE_OUTPUT_PP_LSPEED_LOW:
    case LL_GPIO_PIN_MODE_OUTPUT_PP_MSPEED_LOW:
    case LL_GPIO_PIN_MODE_OUTPUT_PP_HSPEED_LOW:
    case LL_GPIO_PIN_MODE_OUTPUT_OD_LSPEED_LOW:
    case LL_GPIO_PIN_MODE_OUTPUT_OD_MSPEED_LOW:
    case LL_GPIO_PIN_MODE_OUTPUT_OD_HSPEED_LOW:
        gpio->BRR = mask;
        break;
    case LL_GPIO_PIN_MODE_INPUT_PULLUP:
    case LL_GPIO_PIN_MODE_OUTPUT_PP_LSPEED_HIGH:
    case LL_GPIO_PIN_MODE_OUTPUT_PP_MSPEED_HIGH:
    case LL_GPIO_PIN_MODE_OUTPUT_PP_HSPEED_HIGH:
    case LL_GPIO_PIN_MODE_OUTPUT_OD_LSPEED_HIGH:
    case LL_GPIO_PIN_MODE_OUTPUT_OD_MSPEED_HIGH:
    case LL_GPIO_PIN_MODE_OUTPUT_OD_HSPEED_HIGH:
        gpio->BSRR = mask;
        break;
    }

    uint32_t cr_mode;
    switch (mode)
    {
    case LL_GPIO_PIN_MODE_INPUT_FLOAT:
        cr_mode = LL_GPIO_CR_MODER_INPUT_FLOAT;
        break;
    case LL_GPIO_PIN_MODE_INPUT_PULLUP:
    case LL_GPIO_PIN_MODE_INPUT_PULLDOWN:
        cr_mode = LL_GPIO_CR_MODER_INPUT_PULL;
        break;
    case LL_GPIO_PIN_MODE_OUTPUT_PP_LSPEED_LOW:
    case LL_GPIO_PIN_MODE_OUTPUT_PP_LSPEED_HIGH:
        cr_mode = LL_GPIO_CR_MODER_OUTPUT_PP_LSPEED;
        break;
    case LL_GPIO_PIN_MODE_OUTPUT_PP_MSPEED_LOW:
    case LL_GPIO_PIN_MODE_OUTPUT_PP_MSPEED_HIGH:
        cr_mode = LL_GPIO_CR_MODER_OUTPUT_PP_MSPEED;
        break;
    case LL_GPIO_PIN_MODE_OUTPUT_PP_HSPEED_LOW:
    case LL_GPIO_PIN_MODE_OUTPUT_PP_HSPEED_HIGH:
        cr_mode = LL_GPIO_CR_MODER_OUTPUT_PP_HSPEED;
        break;
    case LL_GPIO_PIN_MODE_OUTPUT_OD_LSPEED_LOW:
    case LL_GPIO_PIN_MODE_OUTPUT_OD_LSPEED_HIGH:
        cr_mode = LL_GPIO_CR_MODER_OUTPUT_OD_LSPEED;
        break;
    case LL_GPIO_PIN_MODE_OUTPUT_OD_MSPEED_LOW:
    case LL_GPIO_PIN_MODE_OUTPUT_OD_MSPEED_HIGH:
        cr_mode = LL_GPIO_CR_MODER_OUTPUT_OD_MSPEED;
        break;
    case LL_GPIO_PIN_MODE_OUTPUT_OD_HSPEED_LOW:
    case LL_GPIO_PIN_MODE_OUTPUT_OD_HSPEED_HIGH:
        cr_mode = LL_GPIO_CR_MODER_OUTPUT_OD_HSPEED;
        break;
    case LL_GPIO_PIN_MODE_FUNCTION_PP_LSPEED:
        cr_mode = LL_GPIO_CR_MODER_AF_PP_LSPEED;
        break;
    case LL_GPIO_PIN_MODE_FUNCTION_PP_MSPEED:
        cr_mode = LL_GPIO_CR_MODER_AF_PP_MSPEED;
        break;
    case LL_GPIO_PIN_MODE_FUNCTION_PP_HSPEED:
        cr_mode = LL_GPIO_CR_MODER_AF_PP_HSPEED;
        break;
    case LL_GPIO_PIN_MODE_FUNCTION_OD_LSPEED:
        cr_mode = LL_GPIO_CR_MODER_AF_OD_LSPEED;
        break;
    case LL_GPIO_PIN_MODE_FUNCTION_OD_MSPEED:
        cr_mode = LL_GPIO_CR_MODER_AF_OD_MSPEED;
        break;
    case LL_GPIO_PIN_MODE_FUNCTION_OD_HSPEED:
        cr_mode = LL_GPIO_CR_MODER_AF_OD_HSPEED;
        break;
    case LL_GPIO_PIN_MODE_ANALOG:
        cr_mode = LL_GPIO_CR_MODER_ANALOG;
        break;
    default:
        LOG_ERROR("pin-%lu-%lu: unknow mode", port, pinn);
        return;
    }
    ll_gpio_set_cr(gpio, pinn, cr_mode);

    if (pin && LL_GPIO_PIN_LOCK)
    {
        ll_gpio_lock_pin(gpio, mask);
    }
}

void ll_gpio_write_pin(uint32_t pin, uint32_t value)
{
    uint32_t port = LL_GPIO_PIN_PORT(pin);
    ASSERT(port < ARRAY_SIZE(LL_GPIO_TABLE));
    GPIO_TypeDef *gpio = LL_GPIO_TABLE[port];

    uint32_t pinn = LL_GPIO_PIN_PINN(pin);
    uint32_t mask = BIT(pinn);

    if (value)
    {
        gpio->BSRR = mask;
    }
    else
    {
        gpio->BRR = mask;
    }
}

uint32_t ll_gpio_read_pin(uint32_t pin)
{
    uint32_t port = LL_GPIO_PIN_PORT(pin);
    ASSERT(port < ARRAY_SIZE(LL_GPIO_TABLE));
    GPIO_TypeDef *gpio = LL_GPIO_TABLE[port];

    uint32_t pinn = LL_GPIO_PIN_PINN(pin);
    uint32_t mask = BIT(pinn);

    return gpio->IDR & mask;
}

void ll_afio_remap(uint32_t mask)
{
    AFIO->MAPR |= LL_AFIO_MAPR_SWJ_CFG | mask;
}