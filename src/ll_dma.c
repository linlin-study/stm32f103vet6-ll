#include "ll_dma.h"

__NO_INIT static ll_dma_isr_callback_t ll_dma1_channel1_isr_callback;
__NO_INIT static ll_dma_isr_callback_t ll_dma1_channel2_isr_callback;
__NO_INIT static ll_dma_isr_callback_t ll_dma1_channel3_isr_callback;
__NO_INIT static ll_dma_isr_callback_t ll_dma1_channel4_isr_callback;
__NO_INIT static ll_dma_isr_callback_t ll_dma1_channel5_isr_callback;
__NO_INIT static ll_dma_isr_callback_t ll_dma1_channel6_isr_callback;
__NO_INIT static ll_dma_isr_callback_t ll_dma1_channel7_isr_callback;

__NO_INIT static ll_dma_isr_callback_t ll_dma2_channel1_isr_callback;
__NO_INIT static ll_dma_isr_callback_t ll_dma2_channel2_isr_callback;
__NO_INIT static ll_dma_isr_callback_t ll_dma2_channel3_isr_callback;
__NO_INIT static ll_dma_isr_callback_t ll_dma2_channel4_isr_callback;
__NO_INIT static ll_dma_isr_callback_t ll_dma2_channel5_isr_callback;

struct ll_dma_channel
{
    DMA_Channel_TypeDef *dmach;
    ll_dma_isr_callback_t *callback_ptr;
};

#define LL_DMA_CHANNEL(dma_num, channel_num)                                    \
    {                                                                           \
        .dmach = DMA##dma_num##_Channel##channel_num,                           \
        .callback_ptr = &ll_dma##dma_num##_channel##channel_num##_isr_callback, \
    }

const struct ll_dma_channel LL_DMA1_CHANNEL1 = LL_DMA_CHANNEL(1, 1);
const struct ll_dma_channel LL_DMA1_CHANNEL2 = LL_DMA_CHANNEL(1, 2);
const struct ll_dma_channel LL_DMA1_CHANNEL3 = LL_DMA_CHANNEL(1, 3);
const struct ll_dma_channel LL_DMA1_CHANNEL4 = LL_DMA_CHANNEL(1, 4);
const struct ll_dma_channel LL_DMA1_CHANNEL5 = LL_DMA_CHANNEL(1, 5);
const struct ll_dma_channel LL_DMA1_CHANNEL6 = LL_DMA_CHANNEL(1, 6);
const struct ll_dma_channel LL_DMA1_CHANNEL7 = LL_DMA_CHANNEL(1, 7);

const struct ll_dma_channel LL_DMA2_CHANNEL1 = LL_DMA_CHANNEL(2, 1);
const struct ll_dma_channel LL_DMA2_CHANNEL2 = LL_DMA_CHANNEL(2, 2);
const struct ll_dma_channel LL_DMA2_CHANNEL3 = LL_DMA_CHANNEL(2, 3);
const struct ll_dma_channel LL_DMA2_CHANNEL4 = LL_DMA_CHANNEL(2, 4);
const struct ll_dma_channel LL_DMA2_CHANNEL5 = LL_DMA_CHANNEL(2, 5);

void ll_dma_start(ll_dma_channel_handle_t h, ll_dma_isr_callback_t callback, uint32_t cpar, uint32_t cmar, uint32_t cndtr, uint32_t ccr)
{
    ASSERT(ccr & DMA_CCR_EN);
    DMA_Channel_TypeDef *dmach = h->dmach;

    //if ((dmach->CCR & DMA_CCR_CIRC) == 0)
    //{
    //    if (dmach->CNDTR)
    //    {
    //        LOG_WARN("DMA%d_Channel%d overflow: %d", 0, 0, dmach->CNDTR);
    //        //LOG_INFO("%s: %d", h->overflow_msg, dmach->CNDTR);
    //        while (dmach->CNDTR)
    //            ;
    //    }
    //}
    ASSERT(dmach->CNDTR);

    dmach->CCR = 0;
    *(h->callback_ptr) = callback;
    dmach->CPAR = cpar;
    dmach->CMAR = cmar;
    dmach->CNDTR = cndtr;
    dmach->CCR = ccr;
}

const char *ll_dma_get_periph_name(DMA_Channel_TypeDef *periph)
{
    if (periph == DMA1_Channel1)
        return "DMA1_Channel1";
    if (periph == DMA1_Channel2)
        return "DMA1_Channel2";
    if (periph == DMA1_Channel3)
        return "DMA1_Channel3";
    if (periph == DMA1_Channel4)
        return "DMA1_Channel4";
    if (periph == DMA1_Channel5)
        return "DMA1_Channel5";
    if (periph == DMA1_Channel6)
        return "DMA1_Channel6";
    if (periph == DMA1_Channel7)
        return "DMA1_Channel7";

    if (periph == DMA2_Channel1)
        return "DMA2_Channel1";
    if (periph == DMA2_Channel2)
        return "DMA2_Channel2";
    if (periph == DMA2_Channel3)
        return "DMA2_Channel3";
    if (periph == DMA2_Channel4)
        return "DMA2_Channel4";
    if (periph == DMA2_Channel5)
        return "DMA2_Channel5";

    return "UnknowPeriph";
}

static void ll_dma_isr(ll_dma_channel_handle_t h, uint32_t pos)
{
    DMA_Channel_TypeDef *dmach = h->dmach;
    DMA_TypeDef *dma = (DMA_TypeDef *)((uint32_t)dmach & ~0xFFUL);
    uint32_t flag = dma->ISR >> pos;
    dma->IFCR = DMA_IFCR_CGIF1 << pos;
    if (flag & DMA_ISR_TEIF1)
    {
        LOG_ERROR("%s transfer error", ll_dma_get_periph_name(h->dmach));
    }
    ll_dma_isr_callback_t callback = *h->callback_ptr;
    if (callback)
    {
        callback(flag);
    }
}

#define LL_DMA_ISR(dma_num, channel_num) ll_dma_isr(&LL_DMA##dma_num##_CHANNEL##channel_num, (channel_num - 1) * 4)

__WEAK void DMA1_Channel1_IRQHandler(void)
{
    LL_DMA_ISR(1, 1);
}

__WEAK void DMA1_Channel2_IRQHandler(void)
{
    LL_DMA_ISR(1, 2);
}

__WEAK void DMA1_Channel3_IRQHandler(void)
{
    LL_DMA_ISR(1, 3);
}

__WEAK void DMA1_Channel4_IRQHandler(void)
{
    LL_DMA_ISR(1, 4);
}

__WEAK void DMA1_Channel5_IRQHandler(void)
{
    LL_DMA_ISR(1, 5);
}

__WEAK void DMA1_Channel6_IRQHandler(void)
{
    LL_DMA_ISR(1, 6);
}

__WEAK void DMA1_Channel7_IRQHandler(void)
{
    LL_DMA_ISR(1, 7);
}

__WEAK void DMA2_Channel1_IRQHandler(void)
{
    LL_DMA_ISR(2, 1);
}

__WEAK void DMA2_Channel2_IRQHandler(void)
{
    LL_DMA_ISR(2, 2);
}

__WEAK void DMA2_Channel3_IRQHandler(void)
{
    LL_DMA_ISR(2, 3);
}

__WEAK void DMA2_Channel4_5_IRQHandler(void)
{
    if (DMA2->ISR & DMA_ISR_GIF4)
    {
        LL_DMA_ISR(2, 4);
    }
    if (DMA2->ISR & DMA_ISR_GIF5)
    {
        LL_DMA_ISR(2, 5);
    }
}