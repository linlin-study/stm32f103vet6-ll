#include "ll_common.h"
#include <string.h>

uint32_t ll_fifo_put(struct ll_fifo *fifo, const void *bufferv, uint32_t len)
{
    ASSERT(is_power_of_two(fifo->size));
    uint8_t *buffer = (uint8_t *)bufferv;
    uint32_t l;

    len = min(len, fifo->size - fifo->in + fifo->out);

    /* first put the data starting from fifo->in to buffer end */
    l = min(len, fifo->size - (fifo->in & (fifo->size - 1)));
    memcpy(fifo->buffer + (fifo->in & (fifo->size - 1)), buffer, l);

    /* then put the rest (if any) at the beginning of the buffer */
    memcpy(fifo->buffer, buffer + l, len - l);

    fifo->in += len;

    if (fifo->callback)
    {
        if ((fifo->in - fifo->out) >= fifo->trigger)
        {
            fifo->callback(fifo);
        }
    }
    return len;
}

uint32_t ll_fifo_get(struct ll_fifo *fifo, const void *bufferv, uint32_t len)
{
    ASSERT(is_power_of_two(fifo->size));
    uint8_t *buffer = (uint8_t *)bufferv;
    uint32_t l;

    /* first get the data from fifo->out until the end of the buffer */
    l = min(len, fifo->size - (fifo->out & (fifo->size - 1)));
    memcpy(buffer, fifo->buffer + (fifo->out & (fifo->size - 1)), l);

    /* then get the rest (if any) from the beginning of the buffer */
    memcpy(buffer + l, fifo->buffer, len - l);

    fifo->out += len;

    return len;
}