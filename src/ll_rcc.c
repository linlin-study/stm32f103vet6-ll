#include "ll_rcc.h"

void ll_rcc_enable_ahb(uint32_t mask)
{
    RCC->AHBENR |= mask;
}

void ll_rcc_enable_apb1(uint32_t mask)
{
    RCC->APB1ENR |= mask;
}

void ll_rcc_enable_apb2(uint32_t mask)
{
    RCC->APB2ENR |= mask;
}

void ll_rcc_disable_ahb(uint32_t mask)
{
    RCC->AHBENR &= ~mask;
}

void ll_rcc_disable_apb1(uint32_t mask)
{
    RCC->APB1ENR &= ~mask;
}

void ll_rcc_disable_apb2(uint32_t mask)
{
    RCC->APB2ENR &= ~mask;
}
